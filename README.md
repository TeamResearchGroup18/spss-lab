% Descriptive Statistics labs

Instructions for completing this practical exercise:

#. Compile the lab instructions

    #. If you are reading this _README.md_ file from the web, open a
    Command shell and change to the Git workspace containing this
    file, that you just cloned:

        #. Click the "windows" button, type "command" then open a "DOS
         box" (the black window).
    
        #. At the command prompt, type:

                cd \Users\yourusername\path\to\spss-lab

            _Note:_  replace "\\path\\to" above with the path to the
        directory into which you cloned the _spss-lab_ repository.
	
    #. In the command window, type:
    
            pandoc --standalone -t html -o instructions.html instructions.md
    

    #. Now, just type:
    
            instructions.html
	    
        to view the instructions in a web browser.
	
#. Read and follow the instructions.
 
 
