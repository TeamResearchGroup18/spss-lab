---
documentclass: hitec
fontfamily: opensans
fontsize: 12
author: Dr. John Noll
title: "SPSS exercise: Descriptive Statistics"
subtitle: 7COM1070 -- Team Research and Development Project
...
# Objectives

The objectives of this practical exercise are to be able to run SPSS and
use it to compute descriptive statistics for a dataset.

You will use SPSS to compute descriptive statistics for the 2019
Cricket World Cup batting statitics.

# Tasks

## Obtain the dataset and start SPSS

#. Download the [2019 Cricket World Cup batting statistics dataset](http://jnoll.nfshost.com/7COM1079-fall-19/topics/Research_questions/icc_world_cup_2019_batting_stats.xlsx).

#. Start SPSS.  Click the "Windows" icon, then _IBM SPSS Statistics_,
 then select the most recent version.  You will see the SPSS "splash"
 window.
 
    ![SPSS Splash window](spss-splash.png){height=70%}\
 
    Note: there is a link to SPSS tutorials from this window; consider
reading them after the lab.


## Load and edit the dataset

#. Load the #link(2019 Cricket World Cup batting statistics
 dataset)(topics/Research_questions)(icc_world_cup_2019_batting_stats.xlsx).
 
    From the menubar of the _Data View_ (spreadsheet) window, select  _File -> Open -> Data_:

    ![SPSS File open dialog](spss-file-open.png){height=75%}\

    Specify "Excel" as the "Files of type"

    ![SPSS File open dialog](spss-file-open-xlsx.png){height=75%}\

    You should see a preview of the data:

    ![SPSS File open dialog](spss-file-open-preview.png){height=70%}\

    In the dialog, make to tick the following:

    * Read variable names from first row of data.
    * Percentage of values that determine data type.
    * Ignore hidden rows
    * Remove leading and trailing spaces

#. Edit the dataset.

    The _Data View_ is a spreadsheet showing your dataset:

    ![SPSS Data editor](spss-data-editor.png){height=75%}\


    Hover on the column header to reveal the column data type:

    ![SPSS Data editor](spss-data-editor-var-type.png){height=75%}\





#. Examine data types.

    The _Variable View_ shows the column (variable) primitive types and "measures":

    ![SPSS Variable view](spss-var-editor.png){height=75%}\

    SPSS tries to guess, but often fails to recognize interval values.
Example: "Inns" ("Innings") should be _scale_.

    ![SPSS data view](spss-var-editor-2.png){height=75%}\

    SPSS uses different terminology for variable types from ours:
    
    * _Scale_ is for interval data (numbers with exact differences).
    * _Ordinal_ is for numberic values with ordering, but not exact differences.
    * _Nominal_ is for categorical values (like colors, names, etc.).

    Be sure your variables are specified correctly.


#. Save the dataset.

    After editing the dataset to ensure variable types and measures
    are correct,  save it as type "SPSS Statistics (*.sav):"

    ![SPSS File save dialog](spss-name-dataset.png){height=75%}\


    This will not change the data source Excel spreadsheet.



## Compute Descriptive Statistics

#. Run descriptive statistics for the entire dataset.

    From the _Variable View_ Toolbar, select _Run descriptive statistics_:

    ![SPSS run descriptive statistics](spss-run-desc-stats.png){height=75%}\






    The output contains several "blocks"
    
    * Instructions block, showing the underlying SPSS script used to
      calculate the output.
    * Frequencies block, showing the calculated output:

        * Statistics - number of valid datapoints.
        * datapoint view - shows frequencies of occurrence of each value. 

    The most recent analysis is always at the end of the output.

    By default it will try to analyze every variable, which in the case of
    batting statistics is not what we want (frequencies of batter names is not interesting!)

#. Run descriptive statistics on _one_ variable

    #. Click "Go To Data" (spreadsheet with star icon)
    #. Highlight desired variable's row ("Inns")
    #. Click "Run descriptive statistics" (spreadsheet with $\mu$ icon)


#. Compute the Mode, etc.

    Calculate the _mode_, _quartiles_, etc., from the _output_ window
toolbar, by selecting _Analyze -> Descriptive statistics -> Frequencies_:

    ![SPSS Analyze Frequencies](spss-analyze-freq.png){height=75%}\

    Select the desired variable to analyze:

    ![SPSS Analyze Frequencies - variables dialog](spss-analyze-freq-2.png){height=60%}\

    Then, 

    #. Click _statistics_.
    #. Select the desired statistics to calculate.
    #. Click _Continue,_ then _OK_.

    ![SPSS Analyze Frequencies - output](spss-analyze-freq-output.png){height=95%}\


#. Compute a _histogram_ to see if the data appear to be normally distributed:

    ![SPSS histogram](spss-analyze-freq-hist.png){height=95%}\



## Save the output

#. Close the _output_ window.

    You will be asked if you want to save your output.

#. Give the output file a _meaningful_ name!

    Note: this saves the _output_, not the edited dataset.  
